-- Group 69
-- Kevin Jönsson 010412, Iivo Grimberg 011101, Ryan Jin 011226

-- | The Tetris game (main module)
module Main where

import ConsoleGUI
import Shapes

--------------------------------------------------------------------------------

-- * The code that puts all the piece together

main = runGame tetrisGame

tetrisGame =
  Game
    { startGame = startTetris,
      stepGame = stepTetris,
      drawGame = drawTetris,
      gameInfo = defaultGameInfo prop_Tetris,
      tickDelay = defaultDelay,
      gameInvariant = prop_Tetris
    }

--------------------------------------------------------------------------------

-- * The various parts of the Tetris game implementation

-- | The state of the game
data Tetris = Tetris (Vector, Shape) Shape [Shape]

-- The state consists of three parts:
--   * The position and shape of the falling piece
--   * The well (the playing field), where the falling pieces pile up
--   * An infinite supply of random shapes

-- ** Positions and sizes

type Vector = (Int, Int)

-- | The size of the well
wellSize :: (Int, Int)
wellSize = (wellWidth, wellHeight)

wellWidth = 10

wellHeight = 20

-- | Starting position for falling pieces
startPosition :: Vector
startPosition = (wellWidth `div` 2 - 1, 0)

-- | Vector addition
vAdd :: Vector -> Vector -> Vector
(x1, y1) `vAdd` (x2, y2) = (x1 + x2, y1 + y2)

-- | Move the falling piece into position
place :: (Vector, Shape) -> Shape
place (v, s) = shiftShape v s

-- B4

-- | An invariant that startTetris and stepTetris should uphold
prop_Tetris :: Tetris -> Bool
prop_Tetris t = prop_Shape s && shapeSize w == wellSize
  where
    Tetris (_, s) w _ = t

-- B5

-- | Add black walls around a shape
addWalls :: Shape -> Shape
addWalls s = addVertical (addHorizontal s)

addHorizontal :: Shape -> Shape
addHorizontal s = S [bs ++ row ++ bs | row <- rs]
  where
    rs = rows s
    bs = [(Just Black)]

addVertical :: Shape -> Shape
addVertical s = S (bs ++ rs ++ bs)
  where
    rs = rows s
    bs = [replicate (length (head rs)) (Just Black)]

-- B6


-- | Visualize the current game state. This is what the user will see
-- when playing the game.
drawTetris :: Tetris -> Shape
drawTetris (Tetris (v, p) w _) = addWalls (combine (shiftShape v p) w)

-- | The initial game state
-- C8
startTetris :: [Double] -> Tetris
startTetris rs = Tetris (startPosition, shape1) (emptyShape wellSize) supply
  where
    test' s = allShapes !! (floor (7 * s))
    shape1 : supply = map test' rs
    

-- | React to input. The function returns 'Nothing' when it's game over,
-- and @'Just' (n,t)@, when the game continues in a new state @t@.
-- C2, C6
stepTetris :: Action -> Tetris -> Maybe (Int, Tetris)
stepTetris MoveDown t = tick t
stepTetris MoveLeft t = Just (0, movePiece (-1) t)
stepTetris MoveRight t = Just (0, movePiece 1 t)
stepTetris Rotate t = Just (0, rotatePiece t)
stepTetris _ t = tick t

-- B7

move :: Vector -> Tetris -> Tetris
move (x, y) (Tetris (pos, s) w rs) = Tetris ((vAdd (x, y) pos), s) w rs

-- B8

tick :: Tetris -> Maybe (Int, Tetris)
tick t
  | collision newTetris = dropNewPiece t
  | otherwise = newState
  where
    newState = Just (0, move (0, 1) t)
    Just (_, newTetris) = newState

-- C1

collision :: Tetris -> Bool
collision t =
  overlaps new_s b
    || w + x >= 11
    || x < 0
    || h + y == 21
  where
    Tetris (v, s) b _ = t
    new_s = place (v, s)
    (x, y) = v
    (w, h) = shapeSize s

-- C3

movePiece :: Int -> Tetris -> Tetris
movePiece m t
  | collision newTetris = t
  | otherwise = newTetris
  where
    newTetris = move (m, 0) t

-- C4

rotate :: Tetris -> Tetris
rotate t = Tetris (v, newShape) b ss
  where
    Tetris (v, s) b ss = t
    newShape = rotateShape s

rotatePiece :: Tetris -> Tetris
rotatePiece t
  | collision newTetris = t
  | otherwise = newTetris
  where
    newTetris = rotate t

-- C7, C10

dropNewPiece :: Tetris -> Maybe (Int, Tetris)
dropNewPiece t = Just (cr, Tetris (startPosition, new_s) new_new_b new_ss)
  where
    Tetris (v, s) b ss = t
    old_s = place (v, s)
    new_b = combine old_s b
    (cr, new_new_b) = clearLines new_b
    new_s = head ss
    new_ss = tail ss

-- C9

clearLines :: Shape -> (Int, Shape)
clearLines b = (cr, S (replicate cr addLines ++ b_new))
  where
    isNotBlack sq = sq /= Just Black
    b_ = [filter isNotBlack r | r <- (rows b)]
    b_new = [r | r <- b_, not (isComplete r)] 
    cr = 20 - (length b_new)

isNotBlack' sq = sq /= Just Black

isComplete :: Row -> Bool
isComplete r = and [sq /= Nothing | sq <- r]

addLines :: Row
addLines = replicate 10 Nothing