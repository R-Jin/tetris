{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
-- Group 69
-- Kevin Jönsson 010412, Iivo Grimberg 011101, Ryan Jin 011226

-- | Types and functions for shapes. The list of all tetris pieces.
module Shapes where

import Data.List (transpose)
import Data.Maybe (isNothing)
import Test.QuickCheck

-- * Shapes

type Square = Maybe Colour

data Colour = Black | Red | Green | Yellow | Blue | Purple | Cyan | Grey
  deriving (Eq, Bounded, Enum, Show)

-- | A geometric shape is represented as a list of lists of squares. Each square
-- can be empty or filled with a block of a specific colour.
data Shape = S [Row] deriving (Eq)

type Row = [Square]

rows :: Shape -> [Row]
rows (S rs) = rs

-- * Showing shapes

showShape :: Shape -> String
showShape s = unlines [showRow r | r <- rows s]
  where
    showRow :: Row -> String
    showRow r = [showSquare s | s <- r]

    showSquare Nothing = '.'
    showSquare (Just Black) = '#' -- can change to '█' on linux/mac
    showSquare (Just Grey) = 'g' -- can change to '▓'
    showSquare (Just c) = head (show c)

instance Show Shape where
  show = showShape
  showList ss r = unlines (map show ss) ++ r

-- * The shapes used in the Tetris game

-- | All 7 tetrominoes (all combinations of 4 connected blocks),
-- see <https://en.wikipedia.org/wiki/Tetromino>
allShapes :: [Shape]
allShapes = [S (makeSquares s) | s <- shapes]
  where
    makeSquares = map (map colour)
    colour c =
      lookup
        c
        [ ('I', Red),
          ('J', Grey),
          ('T', Blue),
          ('O', Yellow),
          ('Z', Cyan),
          ('L', Green),
          ('S', Purple)
        ]
    shapes =
      [ [ "I",
          "I",
          "I",
          "I"
        ],
        [ " J",
          " J",
          "JJ"
        ],
        [ " T",
          "TT",
          " T"
        ],
        [ "OO",
          "OO"
        ],
        [ " Z",
          "ZZ",
          "Z "
        ],
        [ "LL",
          " L",
          " L"
        ],
        [ "S ",
          "SS",
          " S"
        ]
      ]

-- * Some simple functions

-- ** A1

emptyShape :: (Int, Int) -> Shape
emptyShape set = S (emptyShape' set)

emptyShape' :: (Int, Int) -> [Row]
emptyShape' (rs, cs) = replicate cs (replicate rs Nothing)

-- ** A2

-- | The size (width and height) of a shape
shapeSize :: Shape -> (Int, Int)
shapeSize (S []) = (0, 0)
shapeSize (S cs) = (length (head cs), length cs)

-- ** A3

-- | Count how many non-empty squares a shape contains
blockCount :: Shape -> Int
blockCount (S cs) = sum [1 | n <- concat cs, n /= Nothing]

-- * The Shape invariant

-- ** A4

-- | Shape invariant (shapes have at least one row, at least one column,
-- and are rectangular)
prop_Shape :: Shape -> Bool
prop_Shape s =
  ( and
      [ length firstRow == length row
        | row <- rows s
      ]
  )
    && row >= 1
    && cols >= 1
  where
    (row, cols) = shapeSize s
    firstRow = head (rows s)

-- * Test data generators

-- ** A5

-- | A random generator for colours
genColour :: Gen Colour
genColour = elements [Black, Red, Green, Yellow, Blue, Purple, Cyan, Grey]

instance Arbitrary Colour where
  arbitrary = genColour

-- ** A6

-- | A random generator for shapes
genShape :: Gen Shape
genShape = elements allShapes

instance Arbitrary Shape where
  arbitrary = genShape

-- * Transforming shapes

-- ** A7

-- | Rotate a shape 90 degrees
rotateShape :: Shape -> Shape
rotateShape s = S (reverse (transpose (rows s)))

-- ** A8

-- | shiftShape adds empty squares above and to the left of the shape
shiftShape :: (Int, Int) -> Shape -> Shape
shiftShape (r, d) s = shiftDown d (shiftRight r s)

shiftRight :: Int -> Shape -> Shape
shiftRight r s = S [ns ++ row | row <- rs]
  where
    rs = rows s
    ns = replicate r Nothing

shiftDown :: Int -> Shape -> Shape
shiftDown d s = S (replicate d (replicate (length (head rs)) Nothing) ++ rs)
  where
    rs = rows s

-- ** A9

-- | padShape adds empty sqaure below and to the right of the shape
padShape :: (Int, Int) -> Shape -> Shape
padShape (l, u) s = shiftUp u (shiftLeft l s)

shiftLeft :: Int -> Shape -> Shape
shiftLeft l s = S [row ++ ns | row <- rs]
  where
    rs = rows s
    ns = replicate l Nothing

shiftUp :: Int -> Shape -> Shape
shiftUp u s = S (rs ++ (replicate u (replicate (length (head rs)) Nothing)))
  where
    rs = rows s

-- ** A10

-- | pad a shape to a given size
padShapeTo :: (Int, Int) -> Shape -> Shape
padShapeTo (dw, dh) s
  | w > 0 && h > 0 = padShape (w, h) s
  | w > 0 = padShape (w, 0) s
  | h > 0 = padShape (0, h) s
  | otherwise = s
  where
    (w_, h_) = shapeSize s
    w = dw - w_
    h = dh - h_

-- * Comparing and combining shapes

-- ** B1

-- | Test if two shapes overlap
overlaps :: Shape -> Shape -> Bool
s1 `overlaps` s2 = or (zipWith rowsOverlap (rows s1) (rows s2))

rowsOverlap :: Row -> Row -> Bool
rowsOverlap r1 r2 = or (zipWith (\e1 e2 -> (e1 /= Nothing) && (e2 /= Nothing)) r1 r2)

-- ** B2

-- | zipShapeWith, like 'zipWith' for lists
zipShapeWith :: (Square -> Square -> Square) -> Shape -> Shape -> Shape
zipShapeWith f s1 s2 = S (zipWith (\sqr1 sqr2 -> zipWith f sqr1 sqr2) (rows s1) (rows s2))

-- ** B3

-- | Combine two shapes. The two shapes should not overlap.
-- The resulting shape will be big enough to fit both shapes.
combine :: Shape -> Shape -> Shape
combine s1 s2 = zipShapeWith combine' (resizeShape s1 s2) (resizeShape s2 s1)
  where
    combine' Nothing Nothing = Nothing
    combine' Nothing sq = sq
    combine' sq Nothing = sq
    combine' sq      se = sq

resizeShape :: Shape -> Shape -> Shape
resizeShape s1 s2 = padShapeTo (shapeSize s2) s1